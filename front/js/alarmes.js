function validaLogin() {
    let userTxt = localStorage.getItem("userLogged");

    if (!userTxt) {
        window.location = "index.html";
    }

}

function logout() {
    localStorage.removeItem("userLogged");
    window.location = "index.html";
}

function voltar() {
    window.location = "dashmenu.html";
}

function gerarRelatorioEventos() {
    let dataini = document.getElementById("dtinicio").value;
    let datafim = document.getElementById("dtfinal").value;

    let dataMsg = {
        dt1: dataini,
        dt2: datafim
    }

    let cabecalho = {
        method: 'POST',
        body: JSON.stringify(dataMsg),
        headers: {
            'Content-type': 'application/json'
        }
    }

    fetch("http://localhost:8080/evento/alarmes", cabecalho)
        .then(res => res.json()) //extrai os dados do retorno 
        .then(result => preencheEventos(result));
          //result é o resultado da extração

}   

function preencheEventos(dados) {
    
    let tabela = '<table class="table table-sm" id="tabeladados"> <tr> <th>Descricao</th> <th>Quantidade</th> </tr>';

    for( i = 0; i < dados.length; i++){
        tabela = tabela + `<tr>
                                <td> ${dados[i].slice(-2,-1)} </td>
                                <td> ${dados[i].slice(-1)} </td>
                            </tr>`
    }

    tabela = tabela + '</table>';
    document.getElementById("tabela").innerHTML = tabela;
    document.getElementById("csv").innerHTML = '<button class="btn btn-secondary" onclick="gerarCsv()">Exportar para CSV</button>';
    document.getElementById("pdf").innerHTML = '<button class="btn btn-secondary" onclick="createPDF()">Exportar para PDF</button>';
    
}

function gerarCsv(){
    let dataini = document.getElementById("dtinicio").value;
    let datafim = document.getElementById("dtfinal").value;
     
    //var dados = document.getElementById("tabeladados");    
    let csv = 'Data de inicio: ' + dataini + '\nData fim: ' + datafim +'\n'
    csv += 'Descricao,Quantidade\n';
    //console.log(dados);
    for( i = 1; i < document.getElementById("tabeladados").rows.length; i++){
        csv += document.getElementById("tabeladados").rows[i].cells[0].innerText;
        csv += ','+ document.getElementById("tabeladados").rows[i].cells[1].innerText;
        csv += '\n';
};
  
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'alarmes.csv';
    hiddenElement.click();
};


function createPDF() {
    let dataini = document.getElementById("dtinicio").value;
    let datafim = document.getElementById("dtfinal").value;
    var sTable = document.getElementById('tabela').innerHTML;

    console.log(sTable);

    var style = "<style>";
    style = style + "table {width: 100%;font: 17px Calibri;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "</style>";

    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');

    win.document.write('<html><head>');
    win.document.write('<title>Profile</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write('Data de inicio: ' + dataini + '\nData fim: ' + datafim);
    win.document.write('<br>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');

    win.document.close(); 	// CLOSE THE CURRENT WINDOW.

    win.print();    // PRINT THE CONTENTS.
}
